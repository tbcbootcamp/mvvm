package com.example.quiz5

interface ApiCallback {
    fun onResponse(response: String) {}
    fun onFailure(response: String) {}
}