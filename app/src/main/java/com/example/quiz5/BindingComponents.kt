package com.example.quiz5

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object BindingComponents {

    @JvmStatic
    @BindingAdapter("setResource")
    fun setImage(view: ImageView, imageUrl: String) {
        Glide.with(view).load(imageUrl).into(view)

    }
}