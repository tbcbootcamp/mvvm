package com.example.quiz5

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val bandsList = mutableListOf<BandModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        adapter = RecyclerViewAdapter(bandsList)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        request(BANDS)

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()
            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                adapter.notifyDataSetChanged()
            }, 2000)
        }
    }

    private fun refresh() {
        adapter.notifyDataSetChanged()
    }

    private fun request(path: String) {
        ApiHandler.getRequest(path, object : ApiCallback {
            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String) {
                parseJson(response)
                adapter.notifyDataSetChanged()

            }
        })
    }

    private fun parseJson(response: String) {
        val json = JSONArray(response)
        for (item in 0 until json.length()) {
            val data = json[item] as JSONObject
            val bands = BandModel()
            bands.name = data.getString("name")
            bands.imgUrl = data.getString("img_url")
            bandsList.add(bands)
        }
    }
}