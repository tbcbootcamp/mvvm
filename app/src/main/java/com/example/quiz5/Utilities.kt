package com.example.quiz5


const val BASE_URL = "http://www.mocky.io/v2/"
const val BANDS = "5ec3ab0f300000850039c29e"
const val HTTP_200_OK = 200
const val HTTP_201_CREATED = 201
const val HTTP_400_BAD_REQUEST = 400
const val HTTP_401_UNAUTHORIZED = 401
const val HTTP_404_NOT_FOUND = 404
const val HTTP_500_INTERNAL_SERVER_ERROR = 500
const val HTTP_204_NO_CONTENT = 204